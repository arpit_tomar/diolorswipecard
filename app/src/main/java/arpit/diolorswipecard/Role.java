package arpit.diolorswipecard;

/**
 * Created by arpit on 27/3/16.
 */
public class Role {

    private String name;
    private int image;

    Role(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }
}
