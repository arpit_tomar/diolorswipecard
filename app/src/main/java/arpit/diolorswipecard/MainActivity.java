package arpit.diolorswipecard;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Role> list;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Add the view via XML
        SwipeFlingAdapterView swipeFlingAdapterView = (SwipeFlingAdapterView) findViewById(R.id.frame);

        //Populate the list
        list = new ArrayList<>();
        list.add(new Role("Role1", R.drawable.image));
        list.add(new Role("Role2", R.drawable.image));
        list.add(new Role("Role3", R.drawable.image));
        list.add(new Role("Role4", R.drawable.image));

        //Set the adapter
        myAdapter = new MyAdapter(this, list);
        swipeFlingAdapterView.setAdapter(myAdapter);

        //Set the listener
        swipeFlingAdapterView.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                Log.d("find", "removed object");
                list.remove(0);
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object o) {
                Toast.makeText(MainActivity.this, "Left!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRightCardExit(Object o) {
                Toast.makeText(MainActivity.this, "Right", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdapterAboutToEmpty(int i) {
                Log.d("find", "onAdapterAboutToEmpty");
            }

            @Override
            public void onScroll(float v) {
                Log.d("find", "onScroll");
            }
        });

        //Set on item click listener
        swipeFlingAdapterView.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int i, Object o) {
                Toast.makeText(MainActivity.this, "Clicked!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

//Adapter for the swipe card
class MyAdapter extends BaseAdapter {

    private ArrayList<Role> al;
    private LayoutInflater inflater;
    private MyHolder holder;

    MyAdapter(Context c, ArrayList<Role> al) {
        
        this.al = al;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return al.size();
    }

    @Override
    public Object getItem(int position) {
        return al.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.item2, parent, false);
            holder = new MyHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }

        holder.textView.setText(al.get(position).getName());
        holder.imageView.setImageResource(al.get(position).getImage());
        return convertView;
    }

    private class MyHolder {
        private TextView textView;
        private ImageView imageView;

        MyHolder(View v) {
            textView = (TextView) v.findViewById(R.id.textview);
            imageView = (ImageView) v.findViewById(R.id.imageview);
        }
    }
}
